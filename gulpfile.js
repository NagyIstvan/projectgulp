var gulp = require('gulp');
var concat = require('gulp-concat');
var imagemin = require('gulp-imagemin');
gulp.task('pack-js', function () {
    return gulp.src(['assets/js/*.js'])
        .pipe(concat('geometry.js'))
        .pipe(gulp.dest('public/build/js'));
 });
gulp.task('pack-img', function () {
    return gulp.src(['assets/image/*.*'])
        .pipe(imagemin())
        .pipe(gulp.dest('public/build/image'));
});

gulp.task('pack-css', function () {
    return gulp.src(['node_modules/bootstrap/dist/css/bootstrap.min.css','assets/css/*.css'])
        .pipe(concat('geometry.css'))
        .pipe(gulp.dest('public/build/css'));
});
gulp.task('watch', function() {
    gulp.watch('assets/js/**/*.js',
    gulp.series('pack-js'));
    gulp.watch('assets/css/**/*.css',
    gulp.series('pack-css'));
    gulp.watch('assets/image/**/*.*',
    gulp.series('pack-img'));
    });

gulp.task('default', gulp.series('watch'));